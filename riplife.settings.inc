<?php
/**
 * @file
 * INC file for riplife.
 */
function riplife_settings_form($form, &$form_state) {
  $form['#tree'] = TRUE;

  $form['default'] = array(
    '#type' => 'fieldset',
    '#title' => t('Mars Settings'), 
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#weight' => 1,
  );
  $form['default']['coordinates'] = array(
    '#type' => 'textfield',
    '#title' => t('Upper-right coordinates.'),
    '#required' => TRUE,
    '#size' => 60,
    '#description' => t('The upper-right coordinates of the plateau.'),
    '#default_value' => variable_get('riplife_coordinates', ''),
  );

  $form['default']['default'] = array(
    '#type' => 'markup',
    '#markup' => t('The lower-left coordinates are assumed to be 0,0.'),
    '#prefix' => '<p>',
    '#suffix' => '</p>',
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Save'));

  return $form;
}

function riplife_settings_form_validate($form, &$form_state) {
  // ASSUMPTIONS: 
  // - Not matching negative numbers - as the lover end of the planet is 0,0
  
  // Check coordinates.
  preg_match("/([0-9]{1,})\ ([0-9]{1,})/", $form_state['values']['default']['coordinates'], $matches);
  // Throw an error if the input is not correct.
  if (empty($matches)) {
    form_set_error('coordinates', t('The upper-right coordinates of the plateau are not correct. <strong>Correct input is for example 0 0</strong>'));
  }
}

function riplife_settings_form_submit($form, &$form_state) {
  // Save all the values.
  variable_set('riplife_coordinates', $form_state['values']['default']['coordinates']);
  // Output message.
  drupal_set_message(t('Settings updated.'));
  // Redirect back to overview page.
  $form_state['redirect'] = 'admin/rovers';
}
