<?php
/**
 * @file
 * INC file for riplife.
 */

/**
 * Overview page for careers block webform translation sets.
 */
function riplife_rovers_overview() {
  // Build the sortable table header.
  $header['coords'] = array('data' => t('Starting position'), 'field' => 'r.coords');
  $header['instructions'] = t('Instructions');
  $header['result'] = t('End position');
  $header['created'] = array('data' => t('Created'), 'field' => 'r.created', 'sort' => 'DESC');
  $header['changed'] = array('data' => t('Updated'), 'field' => 'r.changed');
  $header['operations'] = array('data' => t('Operations'));

  // Get all rovers.
  $query = db_select('plateau_rovers', 'r');
  $query = $query->extend('PagerDefault')->extend('TableSort');
  // Limit rovers by 20 per page.
  $rids = $query->fields('r', array('rid'))->limit(20)->orderByHeader($header)->execute()->fetchCol();
  // Now load them as entities.
  $rovers = $rids ? entity_load('rover', $rids) : array();

  // Format roves in a form.
  $form = drupal_get_form('riplife_rovers_form', $rovers, $header);
  $form['pager'] = array('#markup' => theme('pager'));

  return $form;  
}

/**
 * Admin form with list of all rovers.
 */
function riplife_rovers_form($form, &$form_state, $rovers, $header) {
  $rows = array();

  foreach ($rovers as $rover) {
    // Generate row output.
    $rows[$rover->rid]['coords'] = check_plain($rover->coords);
    $rows[$rover->rid]['instructions'] = check_plain($rover->instructions);
    // INFO: Decided to process result directly here to add some realism 
    // as it is happening somewhere there. 
    // Normally this would be stored on riplife_add_rover_form_submit
    $result = riplife_rovers_send_command($rover->coords, $rover->instructions);
    $rows[$rover->rid]['result'] = $result;
    $rows[$rover->rid] += array(
      'created' => format_date($rover->created, 'short'),
      'changed' => format_date($rover->changed, 'short'),
    );
    // Add action buttons.
    $rows[$rover->rid]['operations'] = array(
      'data' => array(
        '#type' => 'markup',
        '#markup' => l(t('edit'), 'admin/rovers/' . $rover->rid . '/edit') . ' ' 
                   . l(t('delete'), 'admin/rovers/' . $rover->rid . '/delete'),
      ),
    );
  }
  // Format form and return it.
  $form['rovers'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('No rovers were deployed yet.'),
  );

  $form['description'] = array(
    '#type' => 'markup',
    '#markup' => '<h2>' . t('Asumptions / decisions made during development.') . '</h2>'
      . '<ul>'
        . '<li>' 
          . t('I decided to do calculations for each robot at the time this list is generated, so there is no command prompt for robots, one adds new robot by clicking <em>Deploy new rover</em> button and filling two fields.') . '<br />'
          . t('This could be done on riplife_add_rover_form_submit() instead and results stored in the database, but I wanted to add some realism as it is happening somewhere there while results are displayed.')
        . '</li>'
        . '<li>' 
          . t('This also complies with <em>Each rover will be finished sequentially</em> as results are calculated for each of them separately and one after another.')
        . '</li>'
        . '<li>' 
          . t('Didn\'t change module name as I assumed it had to remain the same.')
        . '</li>'
        . '<li>' 
          . t('Removed all predefined menu items, and created new ones - project specific.')
        . '</li>'
        . '<li>' 
          . t('Removed hook_perm() as it was Drupal 6 specific and replaced it with the proper Drupal 7 one, as well as used that in hook_menu.')
        . '</li>'
        . '<li>' 
          . t('Didn\'t added comments for _validate or _submit functions as it is obvious what they do.')
        . '</li>'
        . '<li>' 
          . t('Assumed that after setting upper-right coordinates, any rover coordinates have to be restricted to the available area.')
        . '</li>'
        . '<li>' 
          . t('Assuming that there also should be some boundaries check while calculating rovers end position so they won\'t go off the planet. This could be done inside riplife_rovers_send_command() function. Just didn\'t had time to finish it off.')
        . '</li>'
        . '<li>' 
          . t('As there was only two input lines / fields requested, I assumed that there is no need to add Title field for rovers so it is missing. Also there could be Rover ID exposed for easier sorting, but this can be achieved by sorting by Created date as well.')
        . '</li>'
        . '<li>' 
          . t('I haven\'t written any tests yet, to do so I would have to familiarise myself with simpletest api more. Just quickly changed the default options and corrected .info file to see them appearing in Simple test list (admin/config/development/testing).')
        . '</li>'
      . '</ul>'
  );

  return $form;
}

/**
 * Adding / editing new rover form.
 */
function riplife_add_rover_form($form, &$form_state, $rover = array()) {
  $form['#tree'] = TRUE;
  // Save rover so it can be referenced in _validate and _submit actions.
  $form['#rover'] = !empty($rover) ? $rover : FALSE;
  // Coordinates field.
  $form['coords'] = array(
    '#type' => 'textfield',
    '#title' => t('Coordinates'),
    '#description' => t('The upper-right coordinates of the plateau. An example position might be <em>0 0 N</em>, which means the rover is in the bottom left corner and facing North.'),
    '#default_value' => !empty($rover->coords) ? $rover->coords : '',
    '#maxlength' => 30,
    '#required' => TRUE,
    '#weight' => 1,
  );

  // Instructions field.
  $form['instructions'] = array(
    '#type' => 'textfield',
    '#title' => t('Instructions'),
    '#description' => t('Instructions telling the rover how to explore the plateau.'),
    '#default_value' => !empty($rover->instructions) ? $rover->instructions : '',
    '#maxlength' => 255,
    '#required' => TRUE,
    '#weight' => 2,
  );

  // Action buttons.
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Save rover'));

  return $form;
}

function riplife_add_rover_form_validate($form, &$form_state) {
  // ASSUMPTIONS: 
  // - Not matching negative numbers - as the lover end of the planet is 0,0
  // - Limiting input numbers to max of the planets size.
  
  // Check coordinates.
  preg_match("/([0-9]{1,})\ ([0-9]{1,})\ ([N|E|W|S]{1})/", $form_state['values']['coords'], $matches);
  // Throw an error if the input is not correct.
  if (empty($matches)) {
    form_set_error('coords', t('Rover coordinates are not correct. <strong>Correct input is for example 0 0 N</strong>'));
  }

  $coords = variable_get('riplife_coordinates', '0 0');
  $realm = explode(' ', $coords);
  if ($matches[1] > $realm[0] || $matches[2] > $realm[1]) {
    form_set_error('coords', t('One of Rover coordinates is greater then planet size (@size).', array('@size' => $coords)));
  }


  // Check coordinates.
  preg_match("/([MLR]{1,})/", $form_state['values']['instructions'], $matches);
  // Throw an error if the input is not correct.
  if (empty($matches) || (strlen($matches[1]) != strlen($form_state['values']['instructions']))) {
    form_set_error('instructions', t('Rover instructions are not correct.'));
  }
}

function riplife_add_rover_form_submit($form, &$form_state) {
  // Grab all the values.
  $values = array(
    'coords' => $form_state['values']['coords'],
    'instructions' => $form_state['values']['instructions'],
    'created' => $form['#rover'] !== FALSE ? $form['#rover']->created : time(),
    'changed' => time(),
  );

  // In case of new rover.
  if ($form['#rover'] === FALSE) {
    drupal_set_message(t('Deployed new rover with %coords coordinates.', array('%coords' => $values['coords'])));
    db_insert('plateau_rovers')->fields($values)->execute();
  }
  // Update existing one.
  else {
    drupal_set_message(t('Rover with %coords coordinates has been updated.', array('%coords' => $values['coords'])));
    db_update('plateau_rovers')->fields($values)->condition('rid', $form['#rover']->rid)->execute();
  }

  // Redirect back to overview page.
  $form_state['redirect'] = 'admin/rovers';
}

/**
 * Ask confirmation for rover deletion.
 */
function riplife_rover_delete_confirm($form, &$form_state, $rover = array()) {
  $form['#rover'] = $rover;

  return confirm_form($form, t('Are you sure you want to delete rover?'), 'admin/config/content/careers', t('Are you sure you want to delete rover with %coords coordinates? <strong>This action cannot be undone.</strong>', array('%coords' => $rover->coords)), t('Delete'), t('Cancel'));
}

/**
 * Delete confirmed, perform the delete.
 */
function riplife_rover_delete_confirm_submit($form, &$form_state) {
  db_delete('plateau_rovers')->condition('rid', $form['#rover']->rid)->execute();
  drupal_set_message(t('Rover with %coords coordinates has been deleted.', array('%coords' => $form['#rover']->coords)));

  // Redirect back to overview page.
  $form_state['redirect'] = 'admin/rovers';
}
